FROM openjdk:11
ADD target/docker-spring-demo.jar docker-spring-demo.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "docker-spring-demo.jar"]